package ch.sourcemotion.example.vertx.dagger

import io.vertx.core.Verticle
import io.vertx.core.spi.VerticleFactory
import org.slf4j.LoggerFactory
import javax.inject.Provider

/**
 * Verticle factory that delegates the instantiation of the verticle to the appropriate, given [Provider].
 * The "dagger:" prefix advises Vertx to call this factory to get [Verticle] instances.
 *
 * @param verticleMap Map of [Provider]'s they are provided itself by several Dagger modules. In this example the corresponding modules are [FruitVerticleModule] and [StorePrintVerticleModule]
 */
class DaggerVerticleFactory(private val verticleMap: Map<String, Provider<Verticle>>) : VerticleFactory {

    private val log = LoggerFactory.getLogger(DaggerVerticleFactory::class.java)

    override fun createVerticle(verticleName: String, classLoader: ClassLoader): Verticle {
        val verticle = verticleMap.getOrElse(sanitizeVerticleClassName(verticleName), { throw IllegalStateException("No provider for verticle type $verticleName found") }).get()
        log.info("Verticle for type: $verticleName created")
        return verticle
    }

    private fun sanitizeVerticleClassName(verticleName: String): String = verticleName.substring(verticleName.lastIndexOf(":") + 1)

    override fun prefix(): String = "dagger"
}