package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import io.vertx.core.Vertx
import io.vertx.core.spi.VerticleFactory
import javax.inject.Singleton

/**
 * Test module to provide @Rule controlled Vert.x instance
 */
@Module
class TestVertxModule(private val vertx: Vertx) {

    @Provides
    @Singleton
    fun provideVertx(verticleFactory: VerticleFactory): Vertx {
        vertx.registerVerticleFactory(verticleFactory)
        return vertx
    }
}