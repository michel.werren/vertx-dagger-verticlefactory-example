package ch.sourcemotion.example.vertx.dagger

import io.vertx.core.eventbus.Message
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory

/**
 * [io.vertx.core.Verticle] to print the current count of fruits in the store.
 * This second type of verticle should show how generic / flexible the approach with
 * "daggerized" [io.vertx.core.Verticle] instantiation can be / is.
 */
class StorePrintVerticle(private val store: FruitStore) : CoroutineVerticle() {

    companion object {
        const val ADDRESS = "print-store.cmd"
    }

    private val log = LoggerFactory.getLogger(StorePrintVerticle::class.java)

    override suspend fun start() {
        vertx.eventBus().consumer<Void>(ADDRESS, this::onPrintEvent)
        log.info("Store print verticle created")
    }

    private fun onPrintEvent(msg: Message<Void>) {
        launch {
            log.info("There are ${store.getFruitCount(Fruit.APPLE)} ${Fruit.APPLE.name}'s in the store")
            log.info("There are ${store.getFruitCount(Fruit.RASPBERRY)} ${Fruit.RASPBERRY.name}'s in the store")
            log.info("There are ${store.getFruitCount(Fruit.BANANA)} ${Fruit.BANANA.name}'s in the store")
        }
    }
}