package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Dagger module to provide the standard [FruitStore].
 */
@Module
object FruitStoreModule {

    /**
     * Provides the singleton [FruitStore]
     */
    @Provides
    @Singleton
    fun provideFruitStore(): FruitStore = InMemoryFruitStore()
}