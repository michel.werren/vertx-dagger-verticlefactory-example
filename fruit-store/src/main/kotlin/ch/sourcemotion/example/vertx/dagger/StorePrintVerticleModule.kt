package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey
import io.vertx.core.Verticle

/**
 * Further module that provides a "daggerized" verticle to demonstrate how simple you can deploy additional verticles via Dagger.
 */
@Module
object StorePrintVerticleModule {

    /**
     * Provides the [StorePrintVerticle] instance. Used / called by [ch.sourcemotion.example.vertx.dagger.DaggerVerticleFactory]
     */
    @Provides
    @IntoMap
    @StringKey("ch.sourcemotion.example.vertx.dagger.StorePrintVerticle")
    fun provideStorePrintVerticle(store: FruitStore): Verticle = StorePrintVerticle(store)
}