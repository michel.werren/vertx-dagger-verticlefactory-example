package ch.sourcemotion.example.vertx.dagger

/**
 * Interface of the store
 */
interface FruitStore {
    /**
     * Get the count of the currently stored pieces
     */
    fun getFruitCount(fruit: Fruit): Int

    /**
     * Applies changes to the store
     */
    fun storeFruits(fruit: Fruit, count: Int)
}